﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Diente
    {
        public int idDiente { get; set; }
        public string NombDiente { get; set; }
        public bool NumDiente { get; set; }
        public int Pocision_idPocision { get; set; }
    }
}
