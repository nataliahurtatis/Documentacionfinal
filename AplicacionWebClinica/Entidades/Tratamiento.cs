﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Tratamiento
    {
        public int idTratamiento { get; set; }
        public string NombTrart { get; set; }
        public string DescripTratatmiento { get; set; }
        public decimal ValorTrat { get; set; }
        public string EstadoTrata { get; set; }
    }
}
