﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class SubSeccion
    {
        public int idSubseccion { get; set; }
        public string Nombre_Subseccion { get; set; }
        public bool Estado_Subseccion { get; set; }
        public int Seccion_idSeccion { get; set; }
    }
}
