﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Consulta
    {
        public int idConsulta { get; set; }
        public System.DateTime FechConsulta { get; set; }
        public bool EstaConsult { get; set; }
        public long Empleado_Persona_idPersona { get; set; }
        public string MotConsulta { get; set; }
        public long Paciente_Persona_idPersona { get; set; }
    }
}
