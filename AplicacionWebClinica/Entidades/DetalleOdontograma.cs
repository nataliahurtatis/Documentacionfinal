﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class DetalleOdontograma
    {
        public int Odontograma_idOdontograma { get; set; }
        public int PartesdelDiente_idPartesdelDiente { get; set; }
        public int Convenciones_idConvenciones { get; set; }
    }
}
