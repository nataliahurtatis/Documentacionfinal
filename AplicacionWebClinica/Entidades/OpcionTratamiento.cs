﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class OpcionTratamiento
    {
        public int idOpcionTrat { get; set; }
        public decimal ValorTotalOpcion { get; set; }
        public int CantTratamient { get; set; }
        public int Tratamiento_idTratamiento { get; set; }
    }
}
