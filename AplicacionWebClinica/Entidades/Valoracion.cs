﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Valoracion
    {
        public int idValoracion { get; set; }
        public decimal ValorTotalCotizacion { get; set; }
        public int Consulta_idConsulta { get; set; }
        public int OpcionTratamiento_idOpcionTrat { get; set; }
        public int OpcionTratamiento_idOpcionTrat1 { get; set; }
    }
}
