﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Convenciones
    {
        public int idConvenciones { get; set; }
        public bool EstConvencion { get; set; }
        public string NombConvencion { get; set; }
    }
}
