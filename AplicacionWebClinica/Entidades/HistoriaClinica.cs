﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class HistoriaClinica
    {

        public int idHistoriaClinica { get; set; }
        public System.DateTime FechCreacion { get; set; }
        public bool EstadoHistoria { get; set; }
        public long Paciente_Persona_idPersona { get; set; }
        public long _Persona_Empleado_idPersona { get; set; }
        public bool estadoHC { get; set; }
    
    }
}
