﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Profesion
    {
        public int idProfesion { get; set; }
        public string nombreprofesion { get; set; }
        public bool estado { get; set; }
    }
}
