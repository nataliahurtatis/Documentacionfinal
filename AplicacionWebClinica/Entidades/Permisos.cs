﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Permisos
    {
        public int idPrermisos { get; set; }
        public string NombrePermiso { get; set; }
        public bool estadoPermiso { get; set; }
    }
}
