﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Personas
    {

        public long idPersona { get; set; }
        public string nombre1 { get; set; }
        public string nombre2 { get; set; }
        public string apellido1 { get; set; }
        public string apellido2 { get; set; }
        public string direccion { get; set; }
        public string fechanac { get; set; }
        public string fechregist { get; set; }
        public int codigo { get; set; }
        public int estado { get; set; }
        public int Genero_idGenero { get; set; }
        public int TipoIdent_idTipoIdent { get; set; }
        public int Profesion_idProfesion { get; set; }
        public int Depto_idDepto { get; set; }
        public int Municipio_idMunicipio { get; set; }
    }
}
