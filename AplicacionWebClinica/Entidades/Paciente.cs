﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Paciente
    {
        public int Persona_idPersona { get; set; }
        public string dir_trabajo { get; set; }
        public Nullable<int> tel_trab { get; set; }
        public string nomb_emp_trabaj { get; set; }
        public int fk_Persona_Acudiente { get; set; }
    }
}
