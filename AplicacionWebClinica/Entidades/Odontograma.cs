﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Odontograma
    {
        public int idOdontograma { get; set; }
        public int Evolucion_idEvolucion { get; set; }
        public int TipoOdontograma_idTipoOdontograma { get; set; }
    }
}
