﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Evolucion
    {
        public int idEvolucion { get; set; }
        public string DescrpEvolucion { get; set; }
        public int HistoriaClinica_idHistoriaClinica { get; set; }
        public System.DateTime FechaEvolu { get; set; }
    }
}
