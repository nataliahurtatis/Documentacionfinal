﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Opciones
    {
        public int idOpciones { get; set; }
        public string nombreopcion { get; set; }
        public bool estadopcion { get; set; }
    }
}
