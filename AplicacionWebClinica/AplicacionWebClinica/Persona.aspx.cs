﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AccesoDatos;
using LogicadeNegocio;
using Entidades;
using System.Web.Services;

namespace AplicacionWebClinica
{
    public partial class Persona : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usuario"] != null && Session["contrasenia"] != null)
            {
                DataTable dt = AD_Empleado.verificar_login(Session["usuario"].ToString(), Session["contrasenia"].ToString());
                if (dt.Rows.Count == 0)
                {
                    Response.Write("<script>alert('Debe iniciar Sesión');</script>");
                    Response.Redirect("/Inicio.aspx");
                }
            }
            else
            {
                Response.Write("<script>alert('Debe iniciar Sesión');</script>");
                Response.Redirect("/Inicio.aspx");
            }

            if (!IsPostBack)
            {
                DataTable dtTipo = LN_TipoID.consultar_tipo();
                tipoid.DataSource = dtTipo;
                tipoid.DataTextField = "nobretipoident";
                tipoid.DataValueField = "idTipoIdent";
                tipoid.DataBind();

                DataTable dtGener = LN_Genero.consultarGenero();
                genero.DataSource = dtGener;
                genero.DataTextField = "nombgenero";
                genero.DataValueField = "idGenero";
                genero.DataBind();

                DataTable dtProfes = LN_Profesion.consultar_profesion();
                lprofesion.DataSource = dtProfes;
                lprofesion.DataTextField = "nombreprofesion";
                lprofesion.DataValueField = "idProfesion";
                lprofesion.DataBind();

                DataTable dtDpto = LN_Dpto.consultarDpto();
                ldpto.DataSource = dtDpto;
                ldpto.DataTextField = "nomb_dpto";
                ldpto.DataValueField = "idDepto";
                ldpto.DataBind();
            }
            DataTable dtMunic = LN_Municipio.consultarMunicipio(Convert.ToInt16(ldpto.SelectedValue));
            municipio.DataSource = dtMunic;
            municipio.DataTextField = "nombremunicipio";
            municipio.DataValueField = "idMunicipio";
            municipio.DataBind();
        }
        protected void Button1_Click1(object sender, EventArgs e)
        {
            string[] arr1 = new string[2];
            arr1[0] = this.guardarPersona();
            arr1[1] = this.guardarTelefono();

            if (CONEXION.transaccion(arr1))
            {
                Response.Write("<script>alert('Transacción Exitosa');</script>");
                Response.BufferOutput = true;
            }
            else
            {
                Response.Write("<script>alert('No pudo hacer la transacción');</script>");
                Response.BufferOutput = true;
            }
        }

        public string guardarPersona()
        {
            Personas p = new Personas();
            p.idPersona = Convert.ToInt32(idPersona.Value);
            p.nombre1 = nombre1.Value;
            p.nombre2 = nombre2.Value;
            p.apellido1 = apellido1.Value;
            p.apellido2 = apellido2.Value;
            p.direccion = direccion.Value;
            p.fechanac = naciiento.Value;
            p.fechregist = registro.Value;
            p.estado = 1;
            p.Genero_idGenero = Convert.ToUInt16(genero.SelectedValue);
            p.TipoIdent_idTipoIdent = Convert.ToInt16(tipoid.SelectedValue);
            p.Depto_idDepto = Convert.ToInt16(ldpto.SelectedValue);
            p.Profesion_idProfesion = Convert.ToInt16(lprofesion.SelectedValue);
            p.Municipio_idMunicipio = Convert.ToInt16(municipio.SelectedValue);
            return AD_Persona.InsertarPersona(p);

        }
        public string guardarTelefono()
        {
            string res = "";
            Telefono tel = new Telefono();
            tel.Persona_idPersona = Convert.ToInt32(idPersona.Value);
            tel.estado = 1;
            for (int j = 0; j < 2; j++)
            {
                tel.numerotel = (j == 0) ? telefono1.Value : telefono2.Value;
                res += LN_Telefono.insertarTelefono(tel) + ";";
            }
            return res;


        }



    }
}