﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="HistoriaClinica.aspx.cs" Inherits="AplicacionWebClinica.HistoriaClinica" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contenido" runat="server">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Historia Clínica</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Historia Clínica
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form" runat="server">
                                <div class="form-group">
                                    <label>Fecha de Creación</label>
                                    <input class="form-control" type="date" required>
                                </div>
                                <div class="form-group">
                                    <label>Paciente</label>
                                    <asp:DropDownList ID="paciente" CssClass="form-control" runat="server" required>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label>Acudiente</label>
                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" required>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <fieldset style="border-color: black">
                                        <legend align="left">1.Antecedentes</legend>
                                        <label>1.1 ANTECEDENTES FAMILIARES</label>
                                    </fieldset>
                                    <div class="form-group">
                                        <label>a. Afecciones Cardiacas   </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="afecciomarcardiacas">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>b. Diabetes mellitus</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="diabetesmellitus">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>c. Hipertención</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="hipertension">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>d. Epilpsia</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="epilepsia_fam">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>e. Cáncer</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cancer">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <fieldset style="border-color: black">
                                            <label>1.2 ANTECEDENTES PATOLOGICOS</label>
                                        </fieldset>
                                        <label>a. Hepatitis</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="hepatitis">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>b. Fiebre reumática</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="fiebrereumatica">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>c. Diabetes</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="diabetes">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>d. Ulcera gástrica o hernia atial</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="ulceragastrica">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>e. Epilipcia</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="epilepcia">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>f. Tensión arterial alta</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="tensionalta">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>g. Convulsiones</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="convulsiones">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>h. Alergias</label>
                                        <label class="checkbox-inline">
                                            <input type="text" id="alergias" class="form-control">
                                        </label>
                                        <br />
                                        <label>i. Mareos frecuentes</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="mareos">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>j. Fracturas o accidentes</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="fracturas">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>k. Cicatriza normalmente</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="cicatriza">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>l. ¿Ha tenido infarto de miocardio?</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="infarto">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>m. Ha perdido el conocimiento</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="perdidaconocimiento">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>n. ¿Ha perdido peso últimamente?</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="peridadepeso">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <fieldset style="border-color: black">
                                            <label>1.3 ANTECEDENTES OXICOLOGICOS</label>
                                        </fieldset>
                                        <label>a. Fuma</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="fuma">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>b. Ingiere bebidas alcohólicas</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="bebidasalcoholicas">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>c. Usa drogas</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="drogas">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>d. Alergia a la anestesia o a los vasos conductores</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="alergiaanestesia">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>e. ¿Está tomando algún medicamento?</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="tomandomedicamento">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>f. ¿Es alérgico a algún medicamento?</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="alergiamedicamento">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="text" id="check">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <fieldset style="border-color: black">
                                            <label>1.4 ANTECEDENTES HOSPITALARIOS</label>
                                        </fieldset>
                                        <label>a. Ha resibido transfusiones</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="transfusiones">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="caheckbox">No
                                        </label>
                                        <br />
                                        <label>b. Ha sido hospitalizado</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="hospitalizado">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="text" placeholder="¿Por qué?">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <fieldset style="border-color: black">
                                            <label>1.5 ANTECEDENTES ORALES</label>
                                        </fieldset>
                                        <label>a. Luxación o fractura de mandíbula</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="luxacion">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>b. Amigdalitis</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="amigdalitis">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>c. Infecciones orales o repetición</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="infeccionesorales">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>d. Sufre de mal aliento</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="malaliento">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>e. Le han dado fuegos</label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="Fuegos">Si
                                        </label>
                                        <label class="checkbox-inline">
                                            <input type="checkbox">No
                                        </label>
                                        <br />
                                        <label>f. Frecuencia del cepillado</label>
                                        <label class="checkbox-inline">
                                            <select id="cepillado">
                                                <option selected>1 vez al día</option>
                                                <option>1 vez al día</option>
                                                <option>2 veces al día</option>
                                                <option>3 veces al día</option>
                                                <option>Más de 5 veces</option>
                                            </select>
                                        </label>
                                        <br />
                                        <label>g. Frecuencia seda dental</label>
                                        <label class="checkbox-inline">
                                            <select id="sedadental">
                                                <option selected>1 vez al día</option>
                                                <option>1 vez al día</option>
                                                <option>2 veces al día</option>
                                                <option>3 veces al día</option>
                                                <option>Más de 5 veces</option>
                                            </select>
                                        </label>
                                        <br />
                                        <label>h. Enjuages bucales</label>
                                        <label class="checkbox-inline">
                                            <select id="enjuagues">
                                                <option selected>1 vez al día</option>
                                                <option>1 vez al día</option>
                                                <option>2 veces al día</option>
                                                <option>3 veces al día</option>
                                                <option>Más de 5 veces</option>
                                            </select>
                                        </label>
                                        <label>j. Su última visita al odontólogo</label>
                                        <label class="checkbox-inline">
                                            <input type="date" id="ultimavisita" />
                                        </label>
                                    </div>
                                    <div class="col-lg-6">
                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <fieldset style="border-color: black">
                                    <label>ANTECEDENTES GENERALES</label>
                                </fieldset>
                                <label>1. A.T.M</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="atmnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="atmanormal">Anormal
                                </label>
                                <br />
                                <label>2. Labios</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="labiosnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="labiosanormal">Anormal
                                </label>
                                <br />
                                <label>3. Lengua</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="lenuganormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="lenguanormal">Anormal
                                </label>
                                <br />
                                <label>4. Paladar</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="paladarnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="paladaranormal">Anormal
                                </label>
                                <br />
                                <label>5. Piso de boca</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="pisodebocanormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="pisodebocaanormal">Anormal
                                </label>
                                <br />
                                <label>6. Piso Carrillos</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="pisocarrillosnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="pisocarrillosanormal">Anormal
                                </label>
                                <br />
                                <label>7. Glándulas salivales</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="salivalesnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="salivalesanormal">Anormal
                                </label>
                                <br />
                                <label>8. Maxilares</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="maxilaresnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="maxilaresanormal">Anormal
                                </label>
                                <br />
                                <label>9. Senos Maxilares</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="smaxilaresnorma">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="smaxilaresanormal">Anormal
                                </label>
                                <br />
                                <label>11. Ganglios</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="gangliosnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="gangliosanormal">Anormal
                                </label>
                                <br />
                                <label>12. Oclusión</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="oclusionormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="oclusionanormal">Anormal
                                </label>
                                <br />
                                <label>13. Frenillos</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="fenilosnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="frenillosanormal">Anormal
                                </label>
                                <br />
                                <label>14. Mucosas</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="mucosasnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="mucosasanormal">Anormal
                                </label>
                                <br />
                                <label>15.Encías</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="enciasnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="enciasanormal">Anormal
                                </label>
                                <br />
                                <label>16. Amígdalas</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="amigdalasnormal">Normal 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="amigdalasanormal">Anormal
                                </label>
                            </div>
                            <div class="form-group">
                                <fieldset style="border-color: black">
                                    <label>EXAMEN DENTAL</label>
                                </fieldset>
                                <label>1. Supernumerarios</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="supernumerarios">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox2">No
                                </label>
                                <br />
                                <label>2. Abrasión</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="abrasion">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox3">No
                                </label>
                                <br />
                                <label>3. Manchas- Cambios de color</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="manchas">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox4">No
                                </label>
                                <br />
                                <label>4. Patología pulpar-Abcesos</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox1">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox5">No
                                </label>
                                <br />
                                <label>5. Maloclusiones</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="maloclusiones">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox7">No
                                </label>
                                <br />
                                <label>6. Incluidos</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="incluidos">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox8">No
                                </label>
                                <br />
                                <label>7. Trauma</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="trauma">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox10">No
                                </label>
                                <br />
                                <label>8. Hábitos</label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox6">Si 
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" id="Checkbox9">No
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>
                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</asp:Content>
