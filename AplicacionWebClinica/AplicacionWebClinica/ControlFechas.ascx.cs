﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AplicacionWebClinica
{
    public partial class ControlFechas : System.Web.UI.UserControl
    {

        public DateTime FechaElegida
        {
            get
            {
                if (!string.IsNullOrEmpty(TextBox1.Text))
                    return Convert.ToDateTime(TextBox1.Text);

                return DateTime.MinValue;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                TextBox1.Text = DateTime.Now.ToShortDateString();
                Calendar1.SelectedDate = DateTime.Today;
            }
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            TextBox1.Text = Calendar1.SelectedDate.ToShortDateString();
        }
    }
}