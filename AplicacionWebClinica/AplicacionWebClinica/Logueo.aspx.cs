﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogicadeNegocio;
using System.Data;

namespace AplicacionWebClinica
{
    public partial class Logueo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable dtable = LN_Empleado.verificar_usuario(nomb_user.Value, passw.Value);
            if (dtable.Rows.Count >= 0)
            {

                try
                {
                    Session.Add("usuario", dtable.Rows[0][0].ToString());
                    Session.Add("contrasenia", passw.Value);
                    Response.Redirect("~/Menu.aspx");
                    Session.Timeout = 10000000;


                }
                catch (Exception)
                {
                    Response.Write("<script>alert('Su nombre de usuario o contraseña, es incorrecto');</script>");
                }
            }
            Letras.Visible = true;
        }
    }
}