﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logueo.aspx.cs" Inherits="AplicacionWebClinica.Logueo" %>

<%@ Register Src="ControlFechas.ascx" TagName="fechas" TagPrefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta charset="utf-8">
    <title>Iniciar sesión</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
    <link rel="stylesheet" href="Content/assets/css/reset.css">
    <link rel="stylesheet" href="Content/assets/css/supersized.css">
    <link rel="stylesheet" href="Content/assets/css/style.css">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

</head>

<body>

    <div class="page-container">
        <h1>Bienvenido</h1>
        <asp:Label ID="Letras" Text="Hola" runat="server" Visible="False"></asp:Label>
        <form action="" method="post" runat="server">
            <input type="text" id="nomb_user" name="username" class="username" placeholder="Digite su nombre de usuario..." runat="server">
            <input type="password" id="passw" name="password" class="password" placeholder="Digite su contraseña..." runat="server">
            <asp:Button class="boton" ID="Button1" runat="server" Text="Iniciar" OnClick="Button1_Click" />
            <div class="error"><span>+</span></div>
        </form>

    </div>


    <!-- Javascript -->

    <script src="Content/assets/js/jquery-1.8.2.min.js"></script>
    <script src="Content/assets/js/supersized.3.2.7.min.js"></script>
    <script src="Content/assets/js/supersized-init.js"></script>
    <script src="Content/assets/js/scripts.js"></script>

</body>
</html>
