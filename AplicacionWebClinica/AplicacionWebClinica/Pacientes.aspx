﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="Pacientes.aspx.cs" Inherits="AplicacionWebClinica.Pacientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="contenido" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Paciente</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Datos Personales
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form id="Form1" role="form" runat="server">
                            <div class="col-lg-6">

                                <div class="form-group">
                                    <label>Fecha de Registro</label>
                                    <input class="form-control" runat="server" id="registro" type="date">
                                </div>
                                <div class="form-group">
                                    <label>Tipo de identificación</label>
                                    <asp:DropDownList ID="tipoid" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label>Numero de identificación</label>
                                    <input type="text" class="form-control" runat="server" id="idPersona">
                                </div>
                                <div class="form-group">
                                    <label>Genero</label>
                                    <asp:DropDownList ID="genero" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label>Nombre 1</label>
                                    <input class="form-control" runat="server" id="nombre1">
                                </div>
                                <div class="form-group">
                                    <label>Nombre 2</label>
                                    <input class="form-control" runat="server" id="nombre2">
                                </div>
                                <div class="form-group">
                                    <label>Apellido 1</label>
                                    <input class="form-control" runat="server" id="apellido1">
                                </div>
                                <div class="form-group">
                                    <label>Apellido 2</label>
                                    <input class="form-control" runat="server" id="apellido2">
                                </div>
                                <div class="form-group">
                                    <label>Fecha de Nacimiento</label>
                                    <input class="form-control" runat="server" id="naciiento" type="date">
                                </div>
                                <div class="form-group">
                                    <label>Acudiente</label>
                                    <asp:DropDownList ID="acudiente" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Dirección</label>
                                    <input class="form-control" runat="server" id="direccion">
                                </div>
                                <div class="form-group">
                                    <label>Nombre del lugar de trabajo</label>
                                    <input class="form-control" runat="server" id="lugardetrabajo">
                                </div>
                                <div class="form-group">
                                    <label>Dirección del trabajo</label>
                                    <input class="form-control" runat="server" id="direcciontrabajo">
                                </div>
                                <div class="form-group">
                                    <label>Telefonos</label>
                                    <input class="form-control" runat="server" id="telefono1" placeholder="Telefono 1...">
                                    <input class="form-control" runat="server" id="telefono2" placeholder="Telefono 2...">
                                </div>
                                <div class="form-group">
                                    <label>Telefono del trabajo</label>
                                    <input class="form-control" runat="server" id="telTrabajo">
                                </div>
                                <div class="form-group">
                                    <label>Profesión</label>
                                    <asp:DropDownList ID="lprofesion" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label>Departamento</label>
                                    <asp:DropDownList ID="ldpto" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label>Ciudad</label>
                                    <asp:DropDownList ID="municipio" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <asp:Button CssClass="btn btn-default" ID="Button1" runat="server" Text="Aceptar" />
                            </div>
                        </form>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</asp:Content>
