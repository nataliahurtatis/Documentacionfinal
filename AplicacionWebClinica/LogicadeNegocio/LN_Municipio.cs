﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;
using System.Data;

namespace LogicadeNegocio
{
    public class LN_Municipio
    {
        public static DataTable consultarMunicipio(Int16 idDpto)
        {
            return AD_Municipio.consultarMunicipio(idDpto);
        }
    }
}
