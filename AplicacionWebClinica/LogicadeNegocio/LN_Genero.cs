﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using Entidades;

namespace LogicadeNegocio
{
    public class LN_Genero
    {
        public static DataTable consultarGenero()
        {
            return AD_Genero.consultarGenero();
        }
    }
}
