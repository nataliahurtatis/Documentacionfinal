﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using System.Data;

namespace LogicadeNegocio
{
    public class LN_Empleado
    {
        public static DataTable verificar_usuario(string nom, string pass)
        {
            return AD_Empleado.verificar_login(nom, pass);
        }
    }
}
