﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoDatos
{
    public class AD_Genero
    {
        public static void InsertarGenero(Genero obj)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "insert into genero(idGenero,nombgenero) values ('{0}','{1}')";
            sql = string.Format(sql, Convert.ToInt32(obj.idGenero), obj.nombgenero);
            CONEXION.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);

        }
        public static DataTable consultarGenero()
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql1 = "SELECT * FROM genero ";
            return CONEXION.EjecutarConsulta(sql1, lista, System.Data.CommandType.Text);

        }
        public static void editar_usuario(Genero objeto)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sl = "update usuario set genero='{0}',nombgenero='{1}'";
            sl = string.Format(sl, objeto.idGenero, objeto.nombgenero);
            CONEXION.EjecutarOperacion(sl, lista, System.Data.CommandType.Text);
        }
    }
}
