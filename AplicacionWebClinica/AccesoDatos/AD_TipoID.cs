﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AccesoDatos
{
    public class AD_TipoID
    {
        public static DataTable consultar_TipoID()
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql1 = "select * from tipoident";
            return CONEXION.EjecutarConsulta(sql1, lista, System.Data.CommandType.Text);

        }
    }
}
