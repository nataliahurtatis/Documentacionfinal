﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace AccesoDatos
{
    public class AD_Pacientes
    {
        public static string InsertarPaciente(Paciente obj)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "insert into paciente values ('{0}','{1}','{2}','{3}','{4}')";
            sql = string.Format(sql, obj.Persona_idPersona, obj.dir_trabajo, obj.tel_trab, obj.nomb_emp_trabaj, obj.fk_Persona_Acudiente);
            return sql;
        }
    }
}
