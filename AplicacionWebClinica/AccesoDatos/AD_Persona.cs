﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using MySql.Data.MySqlClient;
using System.Data;

namespace AccesoDatos
{
    public class AD_Persona
    {
        public static string InsertarPersona(Personas obj)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "insert into persona values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','0','{7}','3','{8}','{9}','{10}','{11}','{12}','{13}')";
            sql = string.Format(sql, obj.idPersona, obj.nombre1, obj.nombre2, obj.apellido1, obj.apellido2, obj.direccion, obj.fechanac, obj.fechregist, obj.estado, obj.Genero_idGenero, obj.TipoIdent_idTipoIdent, obj.Profesion_idProfesion, obj.Depto_idDepto, obj.Municipio_idMunicipio);
            return sql;
        }
        public static DataTable consultar_categoria()
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql1 = "SELECT * FROM genero ";
            return CONEXION.EjecutarConsulta(sql1, lista, System.Data.CommandType.Text);

        }
        public static void editar_usuario(Genero objeto)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sl = "update usuario set genero='{0}',nombgenero='{1}'";
            sl = string.Format(sl, objeto.idGenero, objeto.nombgenero);
            CONEXION.EjecutarOperacion(sl, lista, System.Data.CommandType.Text);
        }
    }
}
