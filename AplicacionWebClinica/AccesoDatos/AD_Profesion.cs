﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using MySql.Data.MySqlClient;
using System.Data;

namespace AccesoDatos
{
    public class AD_Profesion
    {
        public static void InsertarProfesion(Profesion obj)
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql = "insert into genero(idProfesion,nombreprofesion,estado) values ('{0}','{1}','{2}')";
            sql = string.Format(sql, Convert.ToInt32(obj.idProfesion), obj.nombreprofesion, obj.estado);
            CONEXION.EjecutarOperacion(sql, lista, System.Data.CommandType.Text);
        }
        public static DataTable consultarProfesion()
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql1 = "SELECT * FROM profesion ";
            return CONEXION.EjecutarConsulta(sql1, lista, System.Data.CommandType.Text);

        }
    }
}
