﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class AD_Dpto
    {
        public static DataTable consultarDpto()
        {
            List<MySqlParameter> lista = new List<MySqlParameter>();
            string sql1 = "SELECT * FROM depto ";
            return CONEXION.EjecutarConsulta(sql1, lista, System.Data.CommandType.Text);

        }
    }
}
